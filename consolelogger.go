/*
Copyright 2020 Ryan Broyles (https://trbroyles.com/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package golog

type ConsoleLogger struct {
	basicLogger
}

func NewConsoleLogger(opts ...LoggingOption) *ConsoleLogger {
	return &ConsoleLogger{
		basicLogger: *newBasicLogger(opts...),
	}
}
