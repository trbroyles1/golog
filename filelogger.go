/*
Copyright 2020 Ryan Broyles (https://trbroyles.com/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package golog

import (
	"log"
	"os"
)

type fileLogger struct {
	basicLogger
	logFile *os.File
}

func NewFileLogger(destFile *os.File, opts ...LoggingOption) *fileLogger {
	lgr := new(fileLogger)
	lgr.basicLogger = *newBasicLogger(opts...)
	lgr.logFile = destFile
	log.SetOutput(lgr.logFile)
	return lgr
}
