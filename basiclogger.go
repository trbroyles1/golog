/*
Copyright 2020 Ryan Broyles (https://trbroyles.com/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package golog

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"runtime"
	"runtime/debug"
	"sync"
	"time"
)

type loggingOptions struct {
	colorizedOutput  bool
	outputLevel      LogLevel
	rotationInterval time.Duration
	lineTerminator   func() string
}

type LoggingOption interface {
	apply(*loggingOptions)
}

type funcLoggingOption struct {
	f func(*loggingOptions)
}

func (fdo *funcLoggingOption) apply(do *loggingOptions) {
	fdo.f(do)
}

func newFuncLoggingOption(f func(*loggingOptions)) *funcLoggingOption {
	return &funcLoggingOption{
		f: f,
	}
}

func WithColorizedOutput() LoggingOption {
	return newFuncLoggingOption(func(lo *loggingOptions) {
		lo.colorizedOutput = true
	})
}

func WithLogLevel(l LogLevel) LoggingOption {
	return newFuncLoggingOption(func(lo *loggingOptions) {
		lo.outputLevel = l
	})
}

func WithLineTerminator(s string) LoggingOption {
	return newFuncLoggingOption(func(lo *loggingOptions) {
		lo.lineTerminator = func() string { return s }
	})
}

func autodetectLineTerminator() string {
	switch runtime.GOOS {
	case "windows":
		return "\r\n"
	default:
		return "\n"
	}
}

var defaultLoggingOptions = loggingOptions{
	colorizedOutput:  false,
	outputLevel:      LevelInfo,
	rotationInterval: 24 * time.Hour,
	lineTerminator:   autodetectLineTerminator,
}

type basicLogger struct {
	logMutex sync.Mutex
	options  *loggingOptions
}

func newBasicLogger(opts ...LoggingOption) *basicLogger {
	myOpts := defaultLoggingOptions
	for _, opt := range opts {
		opt.apply(&myOpts)
	}

	return &basicLogger{
		options: &myOpts,
	}

}

func (me *basicLogger) logMessage(message string) {
	me.logMutex.Lock()
	var stackTrace = debug.Stack()
	var stackTraceCallerLine = bytes.Split(stackTrace, []byte("\n"))[7]
	var stackTraceCallerName = stackTraceCallerLine[:bytes.LastIndex(stackTraceCallerLine, []byte("("))]
	switch me.options.colorizedOutput {
	case true:
		log.Printf("\033[0;96m(%s)\033[0m %s"+me.options.lineTerminator(), string(stackTraceCallerName), message)
	default:
		log.Printf("(%s) %s"+me.options.lineTerminator(), string(stackTraceCallerName), message)
	}

	me.logMutex.Unlock()
}

func (me *basicLogger) SetOutputLevel(level LogLevel) {
	if me.options == nil {
		me.options = &defaultLoggingOptions
	}

	me.options.outputLevel = level
	log.Println("Log level set to ", me.options.outputLevel)
}

func (me *basicLogger) Fatal(message string) {
	switch me.options.colorizedOutput {
	case true:
		me.logMessage(fmt.Sprintf("\033[0;91m[FATAL]\033[0m %s", message))
	default:
		me.logMessage(fmt.Sprintf("[FATAL] %s", message))
	}

	os.Exit(1)
}

func (me *basicLogger) Fatalf(format string, args ...interface{}) {
	switch me.options.colorizedOutput {
	case true:
		me.logMessage(fmt.Sprintf("\033[0;91m[FATAL]\033[0m "+format, args...))
	default:
		me.logMessage(fmt.Sprintf("[FATAL] "+format, args...))
	}

	os.Exit(1)
}

func (me *basicLogger) Panic(message string) {
	switch me.options.colorizedOutput {
	case true:
		me.logMessage(fmt.Sprintf("\033[0;91m[PANIC]\033[0m %s", message))
	default:
		me.logMessage(fmt.Sprintf("[PANIC] %s", message))
	}
	panic(errors.New(message))
}

func (me *basicLogger) Panicf(format string, args ...interface{}) {
	var msg string
	switch me.options.colorizedOutput {
	case true:
		msg = fmt.Sprintf("\033[0;91m[PANIC]\033[0m "+format, args...)
	default:
		msg = fmt.Sprintf("[PANIC] "+format, args...)
	}

	me.logMessage(msg)
	panic(msg)
}

func (me *basicLogger) Err(message string) {
	if me.options.outputLevel >= LevelError {
		switch me.options.colorizedOutput {
		case true:
			me.logMessage(fmt.Sprintf("\033[0;91m[ERROR]\033[0m %s", message))
		default:
			me.logMessage(fmt.Sprintf("[ERROR] %s", message))
		}
	}
}

func (me *basicLogger) Errf(format string, args ...interface{}) {
	if me.options.outputLevel >= LevelError {
		switch me.options.colorizedOutput {
		case true:
			me.logMessage(fmt.Sprintf("\033[0;91m[ERROR]\033[0m "+format, args...))
		default:
			me.logMessage(fmt.Sprintf("[ERROR] "+format, args...))
		}
	}
}

func (me *basicLogger) Warn(message string) {
	if me.options.outputLevel >= LevelWarn {
		switch me.options.colorizedOutput {
		case true:
			me.logMessage(fmt.Sprintf("\033[0;93m[WARN]\033[0m %s", message))
		default:
			me.logMessage(fmt.Sprintf("[WARN] %s", message))
		}
	}
}

func (me *basicLogger) Warnf(format string, args ...interface{}) {
	if me.options.outputLevel >= LevelWarn {
		switch me.options.colorizedOutput {
		case true:
			me.logMessage(fmt.Sprintf("\033[0;93m[WARN]\033[0m "+format, args...))
		default:
			me.logMessage(fmt.Sprintf("[WARN] "+format, args...))
		}
	}
}

func (me *basicLogger) Info(message string) {
	if me.options.outputLevel >= LevelInfo {
		switch me.options.colorizedOutput {
		case true:
			me.logMessage(fmt.Sprintf("\033[0;92m[INFO]\033[0m %s", message))
		default:
			me.logMessage(fmt.Sprintf("[INFO] %s", message))
		}
	}
}

func (me *basicLogger) Infof(format string, args ...interface{}) {
	if me.options.outputLevel >= LevelInfo {
		switch me.options.colorizedOutput {
		case true:
			me.logMessage(fmt.Sprintf("\033[0;92m[INFO]\033[0m "+format, args...))
		default:
			me.logMessage(fmt.Sprintf("[INFO] "+format, args...))
		}
	}
}

func (me *basicLogger) Debug(message string) {
	if me.options.outputLevel >= LevelDebug {
		switch me.options.colorizedOutput {
		case true:
			me.logMessage(fmt.Sprintf("\033[0;97m[DEBUG]\033[0m %s", message))
		default:
			me.logMessage(fmt.Sprintf("[DEBUG] %s", message))
		}
	}
}

func (me *basicLogger) Debugf(format string, args ...interface{}) {
	if me.options.outputLevel >= LevelDebug {
		switch me.options.colorizedOutput {
		case true:
			me.logMessage(fmt.Sprintf("\033[0;97m[DEBUG]\033[0m "+format, args...))
		default:
			me.logMessage(fmt.Sprintf("[DEBUG] "+format, args...))
		}
	}
}

func (me *basicLogger) Init(message string) {
	switch me.options.colorizedOutput {
	case true:
		me.logMessage(fmt.Sprintf("\033[0;94m[INIT]\033[0m %s", message))
	default:
		me.logMessage(fmt.Sprintf("[INIT] %s", message))
	}
}

func (me *basicLogger) Initf(format string, args ...interface{}) {
	switch me.options.colorizedOutput {
	case true:
		me.logMessage(fmt.Sprintf("\033[0;94m[INIT]\033[0m "+format, args...))
	default:
		me.logMessage(fmt.Sprintf("[INIT] "+format, args...))
	}

}
