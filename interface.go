/*
Copyright 2020 Ryan Broyles (https://trbroyles.com/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package golog

type Logger interface {
	Fatal(message string)
	Fatalf(format string, args ...interface{})
	Panic(message string)
	Panicf(format string, args ...interface{})
	Err(message string)
	Errf(format string, args ...interface{})
	Warn(message string)
	Warnf(format string, args ...interface{})
	Info(message string)
	Infof(format string, args ...interface{})
	Debug(message string)
	Debugf(format string, args ...interface{})
	Init(message string)
	Initf(format string, args ...interface{})
	SetOutputLevel(level LogLevel)
}
