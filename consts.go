/*
Copyright 2020 Ryan Broyles (https://trbroyles.com/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package golog

type LogLevel int

const (
	LevelError LogLevel = 1
	LevelWarn  LogLevel = 2
	LevelInfo  LogLevel = 3
	LevelDebug LogLevel = 4
)
