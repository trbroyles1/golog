/*
Copyright 2020 Ryan Broyles (https://trbroyles.com/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package golog

import (
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

func WithRotationInterval(i time.Duration) LoggingOption {
	return newFuncLoggingOption(func(lo *loggingOptions) {
		lo.rotationInterval = i
	})
}

type rotatingTeeLogger struct {
	basicLogger
	logFile              *os.File
	logFileName          string
	lockToConsoleLogging bool
}

func NewRotatingTeeLoggerWithOptions(opts ...LoggingOption) *rotatingTeeLogger {
	baseLogger := newBasicLogger(opts...)
	tl := new(rotatingTeeLogger)
	tl.basicLogger = *baseLogger
	tl.rotateLogFile()
	return tl
}

func NewRotatingTeeLogger(rotationInterval time.Duration) *rotatingTeeLogger {
	return NewRotatingTeeLoggerWithOptions(
		WithRotationInterval(rotationInterval),
	)
}

func (me *rotatingTeeLogger) rotateLogFile() {
	if me.lockToConsoleLogging {
		return
	}
	me.logMutex.Lock()
	defer me.logMutex.Unlock()
	newLogFileName := getLogFileName()
	var newLogFile *os.File
	var err error
	if me.logFile != nil {
		log.Println(fmt.Sprintf("--rotation interval of %s reached, rotating to %s--", me.options.rotationInterval, newLogFileName))
	}

	os.Mkdir("log", 0666)
	if newLogFile, err = os.Create(newLogFileName); err != nil {
		if me.logFile != nil {
			log.Println("!! could not create new log file !! logging continues here.")
			log.Println(err.Error())
			time.AfterFunc(me.options.rotationInterval, me.rotateLogFile)
			return
		}
		log.SetOutput(os.Stdout)
		log.Println("!! couldn't make a new log file !! falling back to console-only logging")
		log.Println(err.Error())
		me.lockToConsoleLogging = true
		return
	}

	log.SetOutput(io.MultiWriter(os.Stdout, newLogFile))
	if me.logFile != nil {
		me.logFile.Close()
		log.Println(fmt.Sprintf("--rotated from %s--", me.logFileName))
	}
	me.logFileName = newLogFileName
	me.logFile = newLogFile
	time.AfterFunc(me.options.rotationInterval, me.rotateLogFile)
}

func getLogFileName() string {
	return fmt.Sprintf("log%clog_%s.log", os.PathSeparator, time.Now().Format("20060102_150405"))
}
