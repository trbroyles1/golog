package main

import (
	"time"

	"gitlab.com/trbroyles1/golog"
)

func main() {
	var logger golog.Logger
	logger = golog.NewRotatingTeeLoggerWithOptions(
		golog.WithLogLevel(golog.LevelDebug),
		golog.WithRotationInterval(10*time.Second),
		golog.WithColorizedOutput(),
	)
	//logger = golog.NewRotatingTeeLogger(10 * time.Second)

	for i := 0; i < 20; i++ {
		logger.Initf("Init round %d", i)
		logger.Init("init msg")
		logger.Debugf("Debug round %d", i)
		logger.Debug("debug msg")
		logger.Infof("Info round %d", i)
		logger.Info("info msg")
		logger.Warnf("Warn round %d", i)
		logger.Warn("warn msg")
		logger.Errf("Err round %d", i)
		logger.Err("err msg")
		//logger.Panicf("panic round %d", i)
		//logger.Panic("panic msg")
		time.Sleep(1 * time.Second)
	}
}
