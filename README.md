# golog

golog is a simple logging library for Go. It is built on top of the basic `log` package provided by the Go stdlib, with the primary goals of adding a few extra nicities:
- Logging levels (Error | Warn | Info | Debug | Init)
- Name of func calling logging function
- Colorized output (optional)
- Log rotation (optional)

The core of golog is the `Logger` interface, defined in `interface.go`:
```
 type Logger interface {
    Fatal(message string) 	
    Fatalf(format string, args ...interface{}) 	
    Panic(message string) 	
    Panicf(format string, args ...interface{}) 	
    Err(message string) 	
    Errf(format string, args ...interface{})
    Warn(message string)
    Warnf(format string, args ...interface{})
    Info(message string)
    Infof(format string, args ...interface{})
    Debug(message string)
    Debugf(format string, args ...interface{})
    Init(message string)
    Initf(format string, args ...interface{})
    SetOutputLevel(level LogLevel) }
```
Users of golog should depend only upon the interface in their code, instantiating and passing the implementation that suits them when starting their application.

Calling `SetOutputLevel` on their interface with one of the `LogLevel`s defined in the `LogLevel` enum will cause the `Logger` to suppress any log messages at a level higher than the specified one.

The log levels are:

```
type LogLevel int

const (
    LevelError LogLevel = 1
    LevelWarn  LogLevel = 2
    LevelInfo  LogLevel = 3
    LevelDebug LogLevel = 4
)

```

Some logging calls to the `Logger` will never be suppressed; namely:
- Either of the `Fatal` funcs. Use these when you need to crash the application
- Either of the `Panic` funcs. Use these when you need to panic
- Either of the `Init` funcs. Use these when you want to log verbose type stuff during your initialization that you do not want suppressed regardless of the `LogLevel` you have specified

golog ships with a few implementations of `Logger`:
- `ConsoleLogger` outputs all messages to the console (stdout). Initialized with the `NewConsoleLogger` func.
- `FileLogger` outputs all messages to a file. Initialized with the `NewFileLogger` func.
- `RotatingTeeLogger` outputs all messages to both a file and stdout, rotating the log file periodically (24 hours by default). Initialized with the `NewRotatingTeeLogger` func.

`RotatingTeeLogger` stores all log files in a subdirectory called `log` within the working directory of the application using the logger. If at any point it is unable to create or rotate the log file, it will fall back to console-only logging.

All of the initialization funcs take as their last argument a variadic argument of `LoggingOption`. Pass these using one or more of the `LoggingOption` funcs:
- `WithColorizedOutput()` - enables colorized logging output
- `WithLogLevel(l LogLevel)` - sets the `LogLevel` the `Logger` will start with
- `WithLineTerminator(s String)` - use if you need to change the line terminator
- `WithRotationInterval(i time.Duration)` - use to set the log rotation interval on a `RotatingTeeLogger`. Ignored on other `Logger`s

An example initialization of `ConsoleLogger` looks like:

```
logger := golog.NewConsoleLogger(WithColorizedOutput(), WithLogLevel(golog.LevelInfo))
```

An example initialization of `RotatingTeeLogger` looks like:

```
loggingOpts := []golog.LoggingOption {
    WithRotationInterval (6 * time.Hour),
    WithLogLevel(golog.LevelWarn),
}

logger := golog.NewRotatingTeeLogger(loggingOpts...)
```
